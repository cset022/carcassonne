>These tiles, while not used in the finished product, were
>created with the intent of making the stage 3 coding easier.
>The idea behind this was that the GUI could ask the players
>where they wanted to place their meeples during their turn,
>and there would be a separate tile graphic for every possible
>location within the tile. By doing this, the tile graphics 
>could be individually assigned to net a certain amount of
>points, and in the case of field placements, certain behaviors
>that would make our code cleaner and hopefully more efficient
>for Stage 3. This idea was completely scrapped upon learning
>that we would not be using our own code for Stage 3, and that
>other groups would not have accounted for this approach. We
>did however include the work that was done, as close to three
>hundred tiles were made during this process.
