package code;

/**
 * Constant variables for the Carcassonne project.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Constants {
	/** 
	 * The number of tiles in the tile set.
	 */
	public static final short TILE_SET_SIZE = 72;
	
	/** 
	 * The CSV file containing the tile set attributes.
	 */
	public static final String TILE_SET_FILE = "TileSet.csv";
	
	/** 
	 * The number of elements specified within a tile.
	 */
	public static final short NUM_ELEMENTS = 5;
	
	/**
	 * The starting tile.
	 */
	public static final Tiles STARTING_TILE = new Tiles(1,2,0,2,2);
	
	/**
	 * TileSet directory.
	 */
	public static final String TILE_SET_DIRECTORY = "TileSet/";
	
	/**
	 * TileSet file format.
	 */
	public static final String TILE_SET_FORMAT = ".gif";
	
	/**
	 * The empty ("null") tile.
	 */
	public static final String NULL_TILE = TILE_SET_DIRECTORY + "null" + TILE_SET_FORMAT;
	
	/**
	 * Meeples directory.
	 */
	public static final String MEEPLES_DIRECTORY = "Meeples/";
	
	/**
	 * Meeples file format.
	 */
	public static final String MEEPLES_FORMAT = ".png";
}
