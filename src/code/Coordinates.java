package code;
/**
 * This class contains the coordinates
 * on the board and the tile that is placed on it.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Coordinates {

	/** 
	 * The x coordinate.
	 */
	private int x;
	
	/** 
	 * The y coordinate.
	 */
	private int y;
	
	/** 
	 * The tile placed on the coordinates.
	 */
	private Tiles t;
	
	/** 
	 * Initializes the x and y coordinates.
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
		this.t = null;
	}
	
	/** 
	 * Initializes the x and y coordinates and the tile placed on it.
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param t the tile to be placed
	 */
	public Coordinates(int x, int y, Tiles t) {
		this.x = x;
		this.y = y;
		this.t = t;
	}
	
	/** 
	 * Adds a tile to the coordinates.
	 * @param t the tile to be placed.
	 * @throws IllegalArgumentException if a tile is already placed at the coordinates.
	 */
	public void addTile(Tiles t) throws IllegalArgumentException {
		if (this.t == null)
			this.t = t;
		else
			throw new IllegalArgumentException(
					"This coordinate already has a tile placed on it.");
	}
	
	/** 
	 * Returns the x coordinate.
	 * @return the x coordinate
	 */
	public int x() { return this.x; }
	
	/** 
	 * Returns the y coordinate.
	 * @return the y coordinate
	 */
	public int y() { return this.y; }
	
	/** 
	 * Returns the the tile placed on the coordinates.
	 * @return the tile placed on the coordinates
	 */
	public Tiles tile() { return this.t; }
	
	/** 
	 * Checks if the x and y coordinates are equal.
	 * @param o object to be compared to
	 * @return <tt>true</tt> if the coordinates are equal
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		
		if (!(o instanceof Coordinates))
			return false;
		
		Coordinates c = (Coordinates) o;
		
		return this.x == c.x && this.y == c.y;
	}
	
	/** 
	 * Returns string "x,y".
	 * @return string "x,y"
	 */
	@Override
	public String toString() {
		return String.valueOf(this.x) + "," + String.valueOf(this.y);
	}
}
