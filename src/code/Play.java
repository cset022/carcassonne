package code;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import gui.Carcassonne;
import gui.MainFrame;

/** 
 * Contains the main method to start the program.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Play {
	/**
	 * Main method to start the program.
	 * @param args names of the players
	 */
	public static void main(String[] args) {
		//If the player names are given through command line arguments, start the game.
		if (args.length > 1 && args.length < 6) {
			Player[] players = new Player[args.length];
			
			for (int i = 0; i < args.length ; i++)
			{
				players[i]= new Player(args[i]);
				System.out.println("Player " + (i+1) + " is " + args[i]);
			}
			
			SwingUtilities.invokeLater(new Carcassonne(players));
		}
		//Else ask for the number of players and their names.
		else {
			SwingUtilities.invokeLater(new Runnable(){
				public void run(){
					JFrame frame = new MainFrame("Carcassonne");
					frame.setSize(1000,500);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
					frame.setVisible(true);				
					
				}
			});
		}
	}
	
}
