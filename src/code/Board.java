package code;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/** 
 * Data structure that functions as the board for tiles to be placed on.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Board implements Iterable<Coordinates> {
	
	/**
	 * Modification count.
	 */
	private int modCount;
	
	/** 
	 * ArrayList containing Coordinates of tiles that has been played and can be played.
	 */
	private ArrayList<Coordinates> board;
	
	/** 
	 * Constructor creates a new ArrayList and adds the coordinates 0,0.
	 */
	public Board() {
		board = new ArrayList<Coordinates>(Constants.TILE_SET_SIZE);
		board.add(new Coordinates(0,0));
	}
	
	/** 
	 * Constructor creates a new ArrayList and adds the starting tile to coordinates 0,0.
	 * @param t the starting tile
	 */
	public Board(Tiles t) {
		Coordinates start = new Coordinates(0,0);
		board = new ArrayList<Coordinates>(Constants.TILE_SET_SIZE);
		board.add(start);
		add(start, t);
	}
	
	/** 
	 * Returns the number of tiles on this board.
	 * @return the number of tiles on this board.
	 */
	public int size() {
		return board.size();
	}
	
	/** 
	 * Adds a tile to the board at the specified Coordinate.
	 * @param coordinate the Coordinates of the tile to be placed.
	 * @param tile the tile to be placed.
	 * @return <tt>true</tt> if the add was successful.
	 */
	public boolean add(Coordinates coordinate, Tiles tile) {
		for (Coordinates c : board) {
			if (c.equals(coordinate)) {
				if (c.tile() == null && check(coordinate, tile)) {
					c.addTile(tile);
					expand(coordinate);
					modCount++;
					return true;
				}
				else
					return false;
			}
		}
		return false;
	}
	
	/**
	 * Returns the Coordinates at the specified index on the board.
	 * @param index index of coordinate to return
	 * @return the Coordinates at the specified index
	 * @throws IndexOutOfBoundsException
	 */
	public Coordinates get(int index) {
		if (index >= size())
			throw new IndexOutOfBoundsException(
					"Index: " + index + "Size: " + size());
		return board.get(index);
	}
	
	/** 
	 * Returns true if the tile is a legal move, else returns false.
	 * @param coordinate the Coordinates of the tile to be placed.
	 * @param tile the tile to be placed.
	 * @return <tt>true</tt> if this tile can be placed on the specified Coordinates.
	 */
	private boolean check(Coordinates coordinate, Tiles tile) {
		Coordinates n = new Coordinates(coordinate.x(), coordinate.y() + 1);
		Coordinates e = new Coordinates(coordinate.x() + 1, coordinate.y());
		Coordinates s = new Coordinates(coordinate.x(), coordinate.y() -1);
		Coordinates w = new Coordinates(coordinate.x() - 1, coordinate.y());
		
		for (Coordinates c : board) {
			Tiles t = c.tile();
			if (c.equals(n)) {
				if (t != null && t.south() != tile.north())
					return false;
			}
			else if (c.equals(e)) {
				if (t != null && t.west() != tile.east())
					return false;
			}
			else if (c.equals(s)) {
				if (t != null && t.north() != tile.south())
					return false;
			}
			else if (c.equals(w)) {
				if (t != null && t.east() != tile.west())
					return false;
			}
		}
		return true;
	}
	
	/** 
	 * Expands the board dynamically to allow for placements adjacent to a newly placed tile.
	 * @param coordinate The Coordinates of the tile that has been placed.
	 */
	private void expand(Coordinates coordinate) {
		boolean expandNorth = true;
		boolean expandEast = true;
		boolean expandSouth = true;
		boolean expandWest = true;
		Coordinates n = new Coordinates(coordinate.x(), coordinate.y() + 1);
		Coordinates e = new Coordinates(coordinate.x() + 1, coordinate.y());
		Coordinates s = new Coordinates(coordinate.x(), coordinate.y() -1);
		Coordinates w = new Coordinates(coordinate.x() - 1, coordinate.y());
		
		for (Coordinates c : board) {
			if (c.equals(n))
				expandNorth = false;
			else if (c.equals(e))
				expandEast = false;
			else if (c.equals(s))
				expandSouth = false;
			else if (c.equals(w))
				expandWest = false;
		}
		
		if (expandNorth)
			board.add(n);
		if (expandEast)
			board.add(e);
		if (expandSouth)
			board.add(s);
		if (expandWest)
			board.add(w);
	}

	/**
     * Returns an iterator over the Coordinates on this board in the sequence placed.
     * @return an iterator over the Coordinates on this board in the sequence placed
     */
	@Override
	public Iterator<Coordinates> iterator() {
		return new Itr();
	}
	
	private class Itr implements Iterator<Coordinates> {
		private int cursor;
		private int expectedModCount;
		
		public Itr() {
			cursor = 0;
			expectedModCount = modCount;
		}

		@Override
		public boolean hasNext() {
			return cursor < size();
		}

		@Override
		public Coordinates next() {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
			if (hasNext())
				return board.get(cursor++);
			
			throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"Removing Coordinates from the board is not allowed.");
		}
	}
}
