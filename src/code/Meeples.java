package code;

/**
 * Class containing the properties of a meeple.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Meeples {
	
	/**
	 * The player number.
	 */
	private int player;
	
	/**
	 * The area in which the meeple is placed.
	 */
	private int area;
	
	/**
	 * Constructor creates a new meeple.
	 * @param player the player number
	 * @param area the area in which the meeple is played
	 */
	public Meeples(int player, int area) {
		this.player = player;
		this.area = area;
	}
	
	/**
	 * Returns the player number.
	 * @return the player number
	 */
	public int player() { return this.player; }
	
	/**
	 * Returns the area in which the meeple was played.
	 * @return the area in which the meeple was played
	 */
	public int getArea() { return this.area; }
	
	/**
	 * Set the area where the meeple is played.
	 * @param area where the meeple is played
	 */
	public void setArea(int area) { this.area = area; }

}
