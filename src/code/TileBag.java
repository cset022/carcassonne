package code;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/** 
 * Class that stores the tile set.
 * Elements for each tile is stored in TileSet.csv located inside the package.
 * Elements are given a corresponding integer value:
 * Field: 0
 * City: 1
 * Road: 2
 * Cloister: 3
 * City with Banner: 4
 * Intersection: 5
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class TileBag {
	
	/** 
	 * ArrayList containing the set of tiles that can be played.
	 */
	private ArrayList<Tiles> tileSet;

	/** 
	 * Constructor creates a new ArrayList of tiles from the TileSet.csv file.
	 */
	public TileBag() {
		tileSet = new ArrayList<Tiles>(Constants.TILE_SET_SIZE - 1);
		
		Scanner scan = null;
		try {
			scan = new Scanner(new FileReader(Constants.TILE_SET_FILE));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while (scan.hasNext()) {
			int[] el = importElements(scan);
			tileSet.add(new Tiles(el[0], el[1], el[2], el[3], el[4]));
		}
		scan.close();
	}
	
	/** 
	 * Imports elements from TileSet.csv.
	 * Returns elements as an int array.
	 * @param scan Scanner object
	 * @return elements as an int array
	 */
	private int[] importElements(Scanner scan) {
		int[] elements = new int[Constants.NUM_ELEMENTS];
		
		String[] line = (scan.nextLine()).split(",");
		
		for (int i = 0; i < elements.length; i++)
			elements[i] = Integer.parseInt(line[i]);
		
		return elements;
	}
	
	/** 
	 * Randomly get a tile from the ArrayList.
	 * Returns null if there are no tiles left.
	 * @return random tile, or <tt>null</tt> if no tiles are left
	 */
	public Tiles getTile() {
		if (this.tileSet.isEmpty())
			return null;
		
		Random r = new Random();
		int i = r.nextInt(this.tileSet.size());
		
		Tiles tile = this.tileSet.get(i);
		this.tileSet.remove(i);
		
		return tile;
	}
	
	/** 
	 * Returns true if the TileBag contains one or more tiles.
	 * @return <tt>true</tt> if the TileBag contains one or more tiles
	 */
	public boolean hasTile() {
		return !this.tileSet.isEmpty();
	}
	
	/** 
	 * Returns true if TileBag is empty.
	 * @return <tt>true</tt> if the TileBag is empty
	 */
	public boolean isEmpty() {
		return this.tileSet.isEmpty();
	}
	
	/** 
	 * Returns the number of elements in the TileBag.
	 * @return the number of elements in the TileBag
	 */
	public int size() {
		return this.tileSet.size();
	}
}
