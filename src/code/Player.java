package code;

/**
 * Class for storing the player's name, points and meeples.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Player {
	
	/**
	 * Player's name.
	 */
	private String name;
	
	/**
	 * Player's points.
	 */
	private int points;
	
	/**
	 * Player's meeples.
	 */
	private int meeples;
	
	/**
	 * Constructor sets the player's name, 
	 * the starting points to 0, and number of meeples to 7.
	 * @param name the player's name
	 */
	public Player(String name) {
		this.name=name;
		this.points=0;
		this.meeples=7;
	}
	
	/**
	 * Returns the player's name.
	 * @return the player's name
	 */
	public String name() { return this.name; }
	
	/**
	 * Returns the number of meeples.
	 * @return the number of meeples
	 */
	public int getMeeples() { return this.meeples; }
	
	/**
	 * Adds a meeple to the player's meeple collection.
	 */
	public void addMeeple() { meeples++; }
	
	/**
	 * Removes a meeple from the player's meeple collection.
	 * @throws NoMeeplesException when the player has no meeples left
	 */
	public void removeMeeple() throws NoMeeplesException {
		if (this.meeples > 0)
			this.meeples--;
		else
			throw new NoMeeplesException();
	}
	
	/**
	 * Returns the player's points.
	 * @return the player's points
	 */
	public int getPoints() { return this.points; }
	
	/**
	 * Add points to the player.
	 * @param points points to add to the player
	 */
	public void addPoints(int points) { this.points += points; }
	
	/**
	 * Custom NoMeeplesException for when the player has no meeples left.
	 * 
	 * @author Paul Go
	 * @author Andrew Kim
	 * @author Jason Caserta
	 * @author Mike Albanese
	 */
	public class NoMeeplesException extends Exception {
		
		/**
		 * Default serial version ID.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Throws NoMeepleException with default message: "Player has no more meeples left".
		 */
		public NoMeeplesException() {
			super("Player has no more meeples left");
		}
		
		/**
		 * Throws NoMeepleException with custom message.
		 * @param message custom NoMeepleException message
		 */
		public NoMeeplesException(String message) {
			super(message);
		}
	}

}
