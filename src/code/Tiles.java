package code;

/** 
 * This class contains the attributes of a tile.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Tiles {
	
	/**
	 * Attribute on north side of tile.
	 */
	private int north;
	
	/**
	 * Attribute on east side of tile.
	 */
	private int east;
	
	/**
	 * Attribute on south side of tile.
	 */
	private int south;
	
	/**
	 * Attribute on west side of tile.
	 */
	private int west;
	
	/**
	 * Attribute on center side of tile.
	 */
	private int center;
	
	/**
	 * Meeple placed on the tile.
	 */
	private Meeples meeple;
	
	/**
	 * String with initial int values of the sides of the tile.
	 */
	private String type;
	
	/**
	 * Number of clockwise rotations.
	 */
	private int clockwiseRotations;
	
	/**
	 * Number of counterclockwise rotations.
	 */
	private int counterclockwiseRotations;

	/** 
	 * Tile class constructor method to set attributes of the tile.
	 * @param north					attribute on north side of tile
	 * @param east					attribute on east side of tile
	 * @param south					attribute on south side of tile
	 * @param west					attribute on west side of tile
	 * @param center				attribute on center side of tile
	 */
	public Tiles(int north, int east, int south, int west, int center) {
		setElements(north, east, south, west, center);
		
		type = Integer.toString(north) +
			Integer.toString(east) +
			Integer.toString(south) +
			Integer.toString(west) +
			Integer.toString(center);
		
		this.clockwiseRotations = 0;
		this.counterclockwiseRotations = 0;
		this.meeple = null;
	}
	
	/** 
	 * Method to set attributes of the tile.
	 * @param north					attribute on north side of tile
	 * @param east					attribute on east side of tile
	 * @param south					attribute on south side of tile
	 * @param west					attribute on west side of tile
	 * @param center				attribute on center side of tile
	 */
	private void setElements(int north, int east, int south, int west, int center) {	
		this.north = north;
		this.east = east;
		this.south = south;
		this.west = west;
		this.center = center;
	}
	
	/**
	 * Get north side of the tile.
	 * @return the north side of the tile
	 */
	public int north(){
		return this.north;
	}
	
	/**
	 * Get east side of the tile.
	 * @return the east side of the tile
	 */
	public int east(){
		return this.east;
	}
	
	/**
	 * Get south side of the tile.
	 * @return the south side of the tile
	 */
	public int south(){
		return this.south;
	}
	
	/**
	 * Get west side of the tile.
	 * @return the west side of the tile
	 */
	public int west(){
		return this.west;
	}
	
	/**
	 * Get center of the tile.
	 * @return the center the tile
	 */
	public int center(){
		return this.center;
	}
	
	/**
	 * Get west side of the tile.
	 * @return the west side of the tile
	 */
	public int clockwiseRotations(){
		return this.clockwiseRotations;
	}
	
	/**
	 * Get center of the tile.
	 * @return the center the tile
	 */
	public int counterclockwiseRotations(){
		return this.counterclockwiseRotations;
	}
	
	/**
	 * Check if attributes are equal.
	 * @param o object to be compared
	 * @return <tt>true</tt> if the tiles are equal
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		
		if (!(o instanceof Tiles))
			return false;
		
		Tiles t = (Tiles) o;
		
		return this.north == t.north
				&& this.east == t.east
				&& this.south == t.south
				&& this.west == t.west
				&& this.center == t.center;
	}
	
	/**
	 * Returns string with initial int values of the sides of the tile.
	 * @return string with initial int values of the sides of the tile
	 */
	@Override
	public String toString() {
		return type;
	}
	
	/**
	 * Rotates tile clockwise.
	 */
	public void rotateClockwise() {
		setElements(this.west, this.north, this.east, this.south, this.center);
		if (this.counterclockwiseRotations > 0)
			this.counterclockwiseRotations--;
		else
			this.clockwiseRotations = ++this.clockwiseRotations == 4 ? 0
					: this.clockwiseRotations;
	}
	
	/**
	 * Rotates tile counter clockwise.
	 */
	public void rotateCounterClockwise() {
		setElements(this.east, this.south, this.west, this.north, this.center);
		if (this.clockwiseRotations > 0)
			this.clockwiseRotations--;
		else
			this.counterclockwiseRotations = ++this.counterclockwiseRotations == 4 ? 0
					: this.counterclockwiseRotations;
	}
	
	/**
	 * Set meeple onto tile.
	 * @param m meeple to set on tile
	 */
	public void setMeeple(Meeples m) { this.meeple = m; }
	
	/**
	 * Returns meeple placed on tile.
	 * @return meeple placed on tile
	 */
	public Meeples getMeeple() { return this.meeple; }
	
	/**
	 * Remove meeple from tile.
	 */
	public void removeMeeple() { this.meeple = null; }
}
