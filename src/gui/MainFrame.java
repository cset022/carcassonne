package gui;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import code.Player;

/**
 * JFrame to ask for the number of players and their names.
 * Sets the players for the game.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class MainFrame extends JFrame{

	/**
	 * Default serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	JButton two = new JButton("Two Players");
	JButton three = new JButton("Three Players");
	JButton four = new JButton("Four Players");
	JButton five = new JButton("Five Players");
	JLabel instructions = new JLabel("ENTER PLAYER ONE'S NAME");
	JLabel instructions2 = new JLabel("ENTER PLAYER TWO'S NAME");
	JLabel instructions3 = new JLabel("ENTER PLAYER THREE'S NAME");
	JLabel instructions4 = new JLabel("ENTER PLAYER FOUR'S NAME");
	JLabel instructions5 = new JLabel("ENTER PLAYER FIVE'S NAME");
	JLabel error = new JLabel("Please fill in every player's name.", SwingConstants.CENTER);
	
	JTextField input = new JTextField(15);
	JTextField input2 = new JTextField(15);
	JTextField input3 = new JTextField(15);
	JTextField input4 = new JTextField(15);
	JTextField input5 = new JTextField(15);
	
	JButton go2 = new JButton("Let's Play!");
	JButton go3 = new JButton("Let's Play!");
	JButton go4 = new JButton("Let's Play!");
	JButton go5 = new JButton("Let's Play!");
	public MainFrame(String title) {
		super(title);

		setLayout(new GridLayout());

		final Container c = getContentPane();
		c.add(two, BorderLayout.WEST);
		c.add(three, BorderLayout.WEST);
		c.add(four, BorderLayout.WEST);
		c.add(five, BorderLayout.WEST);
		
		//two players
		two.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {

				c.removeAll();
				//c.setLayout(new GridLayout(0,2));
				c.setLayout(new GridBagLayout());
				GridBagConstraints gc = new GridBagConstraints();
				
				//First Column
				gc.anchor = GridBagConstraints.LINE_END;
				gc.weightx=1;
				gc.weighty=1;
				
				gc.gridx = 0;
				gc.gridy = 0;
				c.add(instructions, gc);
				
				gc.gridx = 0;
				gc.gridy = 1;
				c.add(instructions2, gc);
				
				//Second Column
				gc.anchor = GridBagConstraints.LINE_START;
				
				gc.gridx=1;
				gc.gridy=0;
				c.add(input, gc);
				
				gc.gridx=1;
				gc.gridy=1;
				c.add(input2, gc);
				
				gc.weightx=.01;
				gc.weighty=.01;
				gc.gridx=1;
				gc.gridy=2;
				c.add(error,gc);
				error.setVisible(false);
				
				gc.weightx=1;
				gc.weighty=1;
				gc.gridx=1;
				gc.gridy=3;
				c.add(go2, gc);
				
				
				repaint();
				revalidate();
			}

		});
		go2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!input.getText().isEmpty()&& !input2.getText().isEmpty()){
					Player one = new Player(input.getText());
					Player two = new Player(input2.getText());					
					Player[] players = new Player[2];
					players[0] = one;
					players[1] = two;
					c.setVisible(false);
					SwingUtilities.invokeLater(new Carcassonne(players));
					dispose();
				}
				else{
					error.setVisible(true);
					repaint();
					revalidate();
				}
			}

		});
		//three players
		three.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.removeAll();
				//c.setLayout(new GridLayout(0,2));
				c.setLayout(new GridBagLayout());
				GridBagConstraints gc = new GridBagConstraints();
				
				//First Column
				gc.anchor = GridBagConstraints.LINE_END;
				gc.weightx=1;
				gc.weighty=1;
				
				gc.gridx = 0;
				gc.gridy = 0;
				c.add(instructions, gc);
				
				gc.gridx = 0;
				gc.gridy = 1;
				c.add(instructions2, gc);
				
				gc.gridx = 0;
				gc.gridy = 2;
				c.add(instructions3, gc);
				
				//Second Column
				gc.anchor = GridBagConstraints.LINE_START;
				
				gc.gridx=1;
				gc.gridy=0;
				c.add(input, gc);
				
				gc.gridx=1;
				gc.gridy=1;
				c.add(input2, gc);
				
				gc.gridx=1;
				gc.gridy=2;
				c.add(input3, gc);
				
				gc.weightx=.01;
				gc.weighty=.01;
				gc.gridx=1;
				gc.gridy=3;
				c.add(error,gc);
				error.setVisible(false);
				
				gc.weightx=1;
				gc.weighty=1;
				gc.gridx=1;
				gc.gridy=4;
				c.add(go3, gc);
				repaint();
				revalidate();
			}

		});
		go3.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!input.getText().isEmpty()&& !input2.getText().isEmpty()&& 
						!input3.getText().isEmpty()){
					Player one = new Player(input.getText());
					Player two = new Player(input2.getText());
					Player three = new Player(input3.getText());
					Player[] players = new Player[3];
					players[0] = one;
					players[1] = two;
					players[2] = three;
					c.setVisible(false);
					SwingUtilities.invokeLater(new Carcassonne(players));
					dispose();
				}
				else{
					c.add(error);
					repaint();
					revalidate();
				}
			}

		});
		//four players
		four.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.removeAll();
				c.setLayout(new GridBagLayout());
				GridBagConstraints gc = new GridBagConstraints();
				
				//First Column
				gc.anchor = GridBagConstraints.LINE_END;
				gc.weightx=1;
				gc.weighty=1;
				
				gc.gridx = 0;
				gc.gridy = 0;
				c.add(instructions, gc);
				
				gc.gridx = 0;
				gc.gridy = 1;
				c.add(instructions2, gc);
				
				gc.gridx = 0;
				gc.gridy = 2;
				c.add(instructions3, gc);
				
				gc.gridx = 0;
				gc.gridy = 3;
				c.add(instructions4, gc);
				
				//Second Column
				gc.anchor = GridBagConstraints.LINE_START;
				
				gc.gridx=1;
				gc.gridy=0;
				c.add(input, gc);
				
				gc.gridx=1;
				gc.gridy=1;
				c.add(input2, gc);
				
				gc.gridx=1;
				gc.gridy=2;
				c.add(input3, gc);
				
				gc.gridx=1;
				gc.gridy=3;
				c.add(input4, gc);
				
				gc.weightx=.01;
				gc.weighty=.01;
				gc.gridx=1;
				gc.gridy=4;
				c.add(error,gc);
				error.setVisible(false);
				
				gc.weightx=1;
				gc.weighty=1;
				gc.gridx=1;
				gc.gridy=5;
				c.add(go4, gc);
				repaint();
				revalidate();
			}

		});
		go4.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!input.getText().isEmpty()&& !input2.getText().isEmpty()&& 
						!input3.getText().isEmpty()&& !input4.getText().isEmpty()){
					Player one = new Player(input.getText());
					Player two = new Player(input2.getText());
					Player three = new Player(input3.getText());
					Player four = new Player(input4.getText());
					Player[] players = new Player[4];
					players[0] = one;
					players[1] = two;
					players[2] = three;
					players[3] = four;
					c.setVisible(false);
					SwingUtilities.invokeLater(new Carcassonne(players));
					dispose();
				}
				else{
					c.add(error);
					repaint();
					revalidate();
				}
			}

		});
		//five players
		five.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.removeAll();
				c.setLayout(new GridBagLayout());
				GridBagConstraints gc = new GridBagConstraints();
				
				//First Column
				gc.anchor = GridBagConstraints.LINE_END;
				gc.weightx=1;
				gc.weighty=1;
				
				gc.gridx = 0;
				gc.gridy = 0;
				c.add(instructions, gc);
				
				gc.gridx = 0;
				gc.gridy = 1;
				c.add(instructions2, gc);
				
				gc.gridx = 0;
				gc.gridy = 2;
				c.add(instructions3, gc);
				
				gc.gridx = 0;
				gc.gridy = 3;
				c.add(instructions4, gc);
				
				gc.gridx = 0;
				gc.gridy = 4;
				c.add(instructions5, gc);
				
				//Second Column
				gc.anchor = GridBagConstraints.LINE_START;
				
				gc.gridx=1;
				gc.gridy=0;
				c.add(input, gc);
				
				gc.gridx=1;
				gc.gridy=1;
				c.add(input2, gc);
				
				gc.gridx=1;
				gc.gridy=2;
				c.add(input3, gc);
				
				gc.gridx=1;
				gc.gridy=3;
				c.add(input4, gc);
				
				gc.gridx=1;
				gc.gridy=4;
				c.add(input5, gc);
				
				gc.weightx=.01;
				gc.weighty=.01;
				gc.gridx=1;
				gc.gridy=5;
				c.add(error,gc);
				error.setVisible(false);
				
				gc.weightx=1;
				gc.weighty=1;
				gc.gridx=1;
				gc.gridy=6;
				c.add(go5, gc);
				repaint();
				revalidate();
			}

		});
		go5.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!input.getText().isEmpty()&& !input2.getText().isEmpty()&& 
						!input3.getText().isEmpty()&& !input4.getText().isEmpty()&& 
						!input5.getText().isEmpty()){
					Player one = new Player(input.getText());
					Player two = new Player(input2.getText());
					Player three = new Player(input3.getText());
					Player four = new Player(input4.getText());
					Player five = new Player(input5.getText());
					Player[] players = new Player[5];
					players[0] = one;
					players[1] = two;
					players[2] = three;
					players[3] = four;
					players[4] = five;
					c.setVisible(false);
					SwingUtilities.invokeLater(new Carcassonne(players));
					dispose();
				}
				else{
					c.add(error);
					repaint();
					revalidate();
				}
			}

		});
	}

}