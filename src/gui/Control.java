package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import code.Constants;
import code.Meeples;
import code.Player.NoMeeplesException;

/**
 * Control bar to rotate and discard tiles, and place meeples on the tile.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Control extends JPanel {

	/**
	 * Default serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * JLabel of the tile image.
	 */
	private JLabel tileImage;
	
	/**
	 * JLabel of the meeple image.
	 */
	private JLabel meepleImage;
	
	/**
	 * Main JPanel.
	 */
	private JPanel basic;
	
	/**
	 * Pointer to the parent JFrame.
	 */
	private Carcassonne parent;

	/**
	 * Constructor sets the parent JFrame.
	 * @param parent pointer the parent JFrame
	 */
	public Control(Carcassonne parent) {
		this.parent = parent;
		basic = new JPanel();
		start();
	}
	
	/**
	 * Gets new tile and resets view and status bar.
	 */
	private void start() {
		parent.getTile();
		parent.getStatusBar().resetView();
		view();
	}
	
	/**
	 * Hands the turn to the next player.
	 */
	public void nextPlayer() {
		parent.nextPlayerTurn();
		start();
	}
	
	/**
	 * Reset the control bar.
	 */
	private void reset() {
		view();
	}
	
	/**
	 * Shows the tile drawn, and the rotate and discard buttons. 
	 */
	private void view() {
		
		basic.removeAll();
		
		basic.setLayout(new BoxLayout(basic, BoxLayout.X_AXIS));
        add(basic);
        
        this.tileImage = TileImage.getTileImage(parent.getCurrentTile());
		basic.add(this.tileImage);
		
		basic.add(Box.createRigidArea(new Dimension(10,0)));
		
		basic.add(rotateButtons());
		
		basic.repaint();
		basic.validate();
	}
	
	/**
	 * Shows the meeple and the buttons to place a meeple, or finish the turn.
	 */
	public void placeMeeple() {
		
		basic.removeAll();
		
		basic.setLayout(new BoxLayout(basic, BoxLayout.X_AXIS));
        add(basic);
        
        this.meepleImage = new JLabel();
        this.meepleImage.setIcon(new ImageIcon(Constants.MEEPLES_DIRECTORY + Integer.toString(parent.getPlayerTurn()) + Constants.MEEPLES_FORMAT));;
		basic.add(this.meepleImage);
        
		basic.add(Box.createRigidArea(new Dimension(10,0)));
		
		basic.add(meepleButtons());
		
		basic.repaint();
		basic.validate();
	}
	
	/**
	 * Generates a JPanel containing the rotate and discard buttons.
	 * @return JPanel containing the rotate and discard buttons
	 */
	private JPanel rotateButtons() {
		JPanel buttons = new JPanel();
		buttons.removeAll();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		
		JButton rotateClockwise = new JButton("Rotate Clockwise");
		buttons.add(rotateClockwise);
		rotateClockwise.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				parent.getCurrentTile().rotateClockwise();
				reset();
			}
			
		});
		
		JButton rotateCounterclockwise = new JButton("Rotate Counterclockwise");
		buttons.add(rotateCounterclockwise);
		rotateCounterclockwise.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				parent.getCurrentTile().rotateCounterClockwise();
				reset();
			}
			
		});
		
		JButton discardTile = new JButton("Discard Tile");
		buttons.add(discardTile);
		discardTile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				start();
			}
			
		});
		
		buttons.repaint();
		buttons.validate();
		return buttons;
	}
	
	/**
	 * Generates a JPanel containing the place meeple and finish turn buttons.
	 * @return JPanel containing the place meeple and finish turn buttons
	 */
	private JPanel meepleButtons() {
		JPanel buttons = new JPanel();
		buttons.removeAll();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		
		JButton addMeeple = new JButton("Add Meeple");
		buttons.add(addMeeple);
		addMeeple.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//placed meeple on area 1 as an example
				parent.getCurrentTile().setMeeple(new Meeples(parent.getPlayerTurn(), 0));
				try {
					parent.getCurrentPlayer().removeMeeple();
				} catch (NoMeeplesException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				nextPlayer();
			}
			
		});
		
		JButton finishTurn = new JButton("Finish Turn");
		buttons.add(finishTurn);
		finishTurn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nextPlayer();
			}
			
		});
		
		buttons.repaint();
		buttons.validate();
		return buttons;
	}
}
