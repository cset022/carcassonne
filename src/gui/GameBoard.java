package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import code.Constants;
import code.Coordinates;

/**
 * Dynamically generates the Carcassonne game board.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class GameBoard extends JPanel {

	/**
	 * Default serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pointer to the parent JFrame.
	 */
	private Carcassonne parent;
	
	/**
	 * Multidimensional array of JLabels containing the tile images representing the board.
	 */
	private JLabel[][] labelHolder;
	
	/**
	 * Maps the x coordinates (key) to the rows of the multidimensional array (value).
	 */
	private Map<Integer, Integer> rows;
	
	/**
	 * Maps the y coordinates (key) to the columns of the multidimensional array (value).
	 */
	private Map<Integer, Integer> cols;

	/**
	 * Constructor sets the parent JFrame.
	 * @param parent pointer the parent JFrame
	 */
	public GameBoard(Carcassonne parent) {
		this.parent = parent;
		resetView();
		
	}
	
	/**
	 * Shows the Carcassonne game board.
	 */
	private void resetView() {
		
		removeAll();
		
		rows = new TreeMap<Integer, Integer>();
		cols = new TreeMap<Integer, Integer>();
		ArrayList<Coordinates> list = new ArrayList<Coordinates>();
		
		//Place coordinates into TreeMap for natural ordering (lowest to highest).
		for (Coordinates c : parent.getBoard()) {
			rows.put(c.x(), null);
			cols.put(c.y(), null);
			list.add(c);
		}
		
		//Translate coordinates to the multidimensional array.
		int iRow = 0, iCol = cols.size();
		for (int key : rows.keySet()) {
			rows.put(key, iRow++);
		}
		for (int key : cols.keySet()) {
			cols.put(key, --iCol);
		}
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		labelHolder = new JLabel[rows.size()][cols.size()];
		
		for (int i = 0; i < rows.size(); i++) {
			for (int j = 0; j < cols.size(); j++) {
				boolean found = false;
				for (int k = 0; k < list.size() && !found; k++) {
					final Coordinates c = list.get(k);
					if (rows.get(c.x()) == i && cols.get(c.y()) == j) {
						//Is an empty space where a tile can be placed. Make into clickable button.
						if (c.tile() == null) {
							String image = Constants.NULL_TILE;
							gbc.gridx = i;
							gbc.gridy = j;
							labelHolder[i][j] = new JLabel();
							labelHolder[i][j].setIcon(new ImageIcon(image));
							add(labelHolder[i][j], gbc);
							found = true;
							list.remove(k);
							labelHolder[i][j].addMouseListener(new MouseAdapter(){
								public void mousePressed(MouseEvent e) {
									boolean added = parent.getBoard().add(c, parent.getCurrentTile());
									if (added) {
										if (parent.getCurrentPlayer().getMeeples() > 0) {
											parent.getControl().placeMeeple();
										}
										else {
											parent.getControl().nextPlayer();
										}
									}
									
									resetView();
								}
							});
						}
						//The coordinates contain a tile. Show tile image.
						else {
							gbc.gridx = i;
							gbc.gridy = j;
							labelHolder[i][j] = TileImage.getTileImage(c.tile());
							add(labelHolder[i][j], gbc);
							found = true;
							list.remove(k);
						}
					}
				}
				//Coordinates have not been opened yet. Empty JLabel.
				if (!found) {
					gbc.gridx = i;
					gbc.gridy = j;
					labelHolder[i][j] = new JLabel();
					add(labelHolder[i][j], gbc);
				}
			}
		}
		repaint();
		revalidate();
	}
	

}
