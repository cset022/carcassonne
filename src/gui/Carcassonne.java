package gui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import code.Board;
import code.Constants;
import code.Player;
import code.TileBag;
import code.Tiles;

/**
 * Parent JFrame for the Carcassonne game.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class Carcassonne extends JFrame implements Runnable {

	/**
	 * Default serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Array of players.
	 */
	private Player[] players;
	
	/**
	 * Index of the current player.
	 */
	private int playerTurn = 0;
	
	/**
	 * Pointer to the current player.
	 */
	private Player currentPlayer;
	
	/**
	 * TileBag containing the tiles yet to be played.
	 */
	private TileBag tileBag;
	
	/**
	 * Back-end board data structure.
	 */
	private Board board = new Board(Constants.STARTING_TILE);
	
	/**
	 * Current tile to be played.
	 */
	private Tiles currentTile;
	
	/**
	 * Main JPanel.
	 */
	private JPanel basic;
	
	/**
	 * Control bar JPanel.
	 */
	private Control control;
	
	/**
	 * Game board JPanel.
	 */
	private GameBoard gameBoard;
	
	/**
	 * Status bar JPanel.
	 */
	private StatusBar statusBar;
	
	/**
	 * Constructor sets the players.
	 * @param players an array of players.
	 */
	public Carcassonne(Player[] players) {
		this.players = players;
		this.currentPlayer = players[0];
		this.tileBag = new TileBag();
	}
	
	@Override
	public void run() {
		setTitle("Carcassonne");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.basic = new JPanel();
        this.basic.setLayout(new BoxLayout(this.basic, BoxLayout.Y_AXIS));
        add(this.basic);
		
		this.statusBar = new StatusBar(this);
		
		this.gameBoard = new GameBoard(this);
		this.basic.add(gameBoard);
		
		this.basic.add(Box.createVerticalGlue());
		
		this.control = new Control(this);
		this.basic.add(control);
		
		this.basic.add(statusBar);
		
		pack();
		setVisible(true);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
	
	/**
	 * Returns a pointer to the board data structure.
	 * @return a pointer to the board data structure
	 */
	public Board getBoard() { return this.board; }
	
	/**
	 * Returns a pointer to the control bar.
	 * @return a pointer to the control bar
	 */
	public Control getControl() { return this.control; }
	
	/**
	 * Returns a pointer to the game board panel.
	 * @return a pointer to the game board panel
	 */
	public GameBoard getGameBoard() { return this.gameBoard; }
	
	/**
	 * Returns a pointer to the status bar.
	 * @return a pointer to the status bar
	 */
	public StatusBar getStatusBar() { return this.statusBar; }
	
	/**
	 * Gets a new tile from the tile bag.
	 */
	public void getTile() { this.currentTile = this.tileBag.getTile(); }
	
	/**
	 * Returns a pointer to the current tile in play.
	 * @return a pointer to the current tile in play
	 */
	public Tiles getCurrentTile() { return this.currentTile; }
	
	/**
	 * Returns a pointer to the tile bag.
	 * @return a pointer to the tile bag
	 */
	public TileBag getTileBag() { return this.tileBag; }
	
	/**
	 * Returns a pointer to the current player.
	 * @return a pointer to the current player
	 */
	public Player getCurrentPlayer() { return this.currentPlayer; }
	
	/**
	 * Returns a pointer to the index of the current player.
	 * @return a pointer to the index of the current player
	 */
	public int getPlayerTurn() { return this.playerTurn; }
	
	/**
	 * Sets the turn to the next player.
	 */
	public void nextPlayerTurn() {
		this.playerTurn = ++this.playerTurn == this.players.length ? 0 : this.playerTurn;
		this.currentPlayer = this.players[this.playerTurn];
	}
	
}
