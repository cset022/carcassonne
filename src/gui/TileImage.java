package gui;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import code.Constants;
import code.Tiles;

/**
 * Methods for getting and creating an JLabel the tile and rotating the image.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class TileImage {
	
	/**
	 * Returns a JLabel of the tile image.
	 * @param tile tile to be made into a JLabel
	 * @return JLabel of the tile image
	 */
	public static JLabel getTileImage(Tiles tile) {
		JLabel label = new JLabel();
		ImageIcon image = new ImageIcon(Constants.TILE_SET_DIRECTORY + tile.toString() + Constants.TILE_SET_FORMAT);
		if (tile.clockwiseRotations() != 0) {
			for (int i = 0; i < tile.clockwiseRotations(); i++) {
				image = rotateClockwise(label, image);
			}
		}
		if (tile.counterclockwiseRotations() != 0) {
			for (int i = 0; i < tile.counterclockwiseRotations(); i++) {
				image = rotateCounterclockwise(label, image);
			}
		}
		label.setIcon(image);
		return label;
	}
	
	/**
	 * Rotates the JLabel ImageIcon clockwise.
	 * @param label the JLabel
	 * @param image the ImageIcon
	 * @return clockwise rotated ImageIcon
	 */
	private static ImageIcon rotateClockwise(JLabel label, ImageIcon image) {
		int w = image.getIconWidth();
		int h = image.getIconHeight();
		int type = BufferedImage.TYPE_INT_RGB;
		
        BufferedImage bufImg = new BufferedImage(h, w, type);
        Graphics2D g2 = bufImg.createGraphics();
        double x = (h - w)/2.0;
        double y = (w - h)/2.0;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(Math.toRadians(90), w/2.0, h/2.0);
        g2.drawImage(image.getImage(), at, label);
        g2.dispose();
        
        return new ImageIcon(bufImg);
	}
	
	/**
	 * Rotates the JLabel ImageIcon counterclockwise.
	 * @param label the JLabel
	 * @param image the ImageIcon
	 * @return counterclockwise rotated ImageIcon
	 */
	private static ImageIcon rotateCounterclockwise(JLabel label, ImageIcon image) {
		int w = image.getIconWidth();
		int h = image.getIconHeight();
		int type = BufferedImage.TYPE_INT_RGB;
		
        BufferedImage bufImg = new BufferedImage(h, w, type);
        Graphics2D g2 = bufImg.createGraphics();
        double x = (h - w)/2.0;
        double y = (w - h)/2.0;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(Math.toRadians(-90), w/2.0, h/2.0);
        g2.drawImage(image.getImage(), at, label);
        g2.dispose();
        
        return new ImageIcon(bufImg);
	}
}
