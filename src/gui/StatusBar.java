package gui;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Status bar showing the player's turn, points, meeples, and the remaining tiles left.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class StatusBar extends JPanel {

	/**
	 * Default serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Pointer to the parent JFrame.
	 */
	private Carcassonne parent;
	
	/**
	 * Constructor sets the parent JFrame.
	 * @param parent pointer the parent JFrame
	 */
	public StatusBar(Carcassonne parent) {
		this.parent = parent;
		view();
	}
	
	/**
	 * Resets the view of the StatusBar.
	 */
	public void resetView() {
		view();
	}
	
	/**
	 * Resets the view of the status bar with additional message.
	 * @param message additional message to be displayed
	 */
	public void resetView(String message) {
		view();
		JLabel msg = new JLabel(message);
		add(msg);
		repaint();
		revalidate();
	}

	/**
	 * Shows the player stats.
	 */
	private void view() {
		removeAll();
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		JLabel playerTurn = new JLabel("PLAYER: " + parent.getCurrentPlayer().name());
		JLabel playerPoints = new JLabel("POINTS: " + parent.getCurrentPlayer().getPoints());
		JLabel playerMeeples = new JLabel("MEEPLES: " + parent.getCurrentPlayer().getMeeples());
		JLabel tilesLeft = new JLabel("TILES LEFT: " + parent.getTileBag().size());
		add(playerTurn);
		add(Box.createRigidArea(new Dimension(10,0)));
		add(playerPoints);
		add(Box.createRigidArea(new Dimension(10,0)));
		add(playerMeeples);
		add(Box.createRigidArea(new Dimension(10,0)));
		add(tilesLeft);
		
		repaint();
		revalidate();
	}
}
