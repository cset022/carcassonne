package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Meeples;
import code.Tiles;

public class MeepleTest {
	//test constructor and player, area getters
	@Test
	public void test00() {
		Meeples m = new Meeples(0,0);
		int expectedPlayer = 0;
		int expectedArea = 0;
		assertTrue("Expected player: " + expectedPlayer + " Actual player: " + m.player() +
				"Expected area: " + expectedArea + " Actual area: " + m.getArea(),
				m.player() == expectedPlayer && m.getArea() == expectedArea);
	}
	//test setArea
	@Test
	public void test01() {
		Meeples m = new Meeples(0,0);
		m.setArea(3);
		int expectedPlayer = 0;
		int expectedArea = 3;
		assertTrue("Expected player: " + expectedPlayer + " Actual player: " + m.player() +
				"Expected area: " + expectedArea + " Actual area: " + m.getArea(),
				m.player() == expectedPlayer && m.getArea() == expectedArea);
	}
	//test setMeeple on a tile
	@Test
	public void test02() {
		Tiles t = new Tiles(0,0,0,0,0);
		Meeples meeple = new Meeples(2,1);
		t.setMeeple(meeple);
		Meeples m = t.getMeeple();
		int expectedPlayer = 2;
		int expectedArea = 1;
		assertTrue("Expected player: " + expectedPlayer + " Actual player: " + m.player() +
				"Expected area: " + expectedArea + " Actual area: " + m.getArea(),
				m.player() == expectedPlayer && m.getArea() == expectedArea);
	}
	//test removeMeeple from a tile
	@Test
	public void test03() {
		Tiles t = new Tiles(0,0,0,0,0);
		Meeples meeple = new Meeples(4,7);
		t.setMeeple(meeple);
		t.removeMeeple();
		Meeples actual = t.getMeeple();
		Meeples expected = null;
		assertTrue("Expected meeple: " + expected + " Actual meeple: " + actual,
				expected == actual);
	}

}
