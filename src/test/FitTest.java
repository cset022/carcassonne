package test;

import org.junit.Test;

import org.junit.Assert;

import code.Board;
import code.Coordinates;
import code.Tiles;

public class FitTest {
	public static Board board = new Board();
	
	//Test if initial tile can be placed at at illegal coordinate 1,1
	@Test public void test01() {
		
		Tiles su = new Tiles(0,0,0,0,0);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(1,1), su);
		
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if initial tile can be placed at 0,0 and expands board.
	@Test public void test02() {
		
		Tiles su = new Tiles(0,0,0,0,0);
		
		boolean added = board.add(new Coordinates(0,0), su);
		int expected = 5;
		int actual = board.size();
		
		Assert.assertTrue("Expected: " + expected + " Actual: " + actual + " Added: " + added,
				expected == actual && added);
	}
	
	//Test if another tile can be placed at 0,0 when another tile is already placed.
	@Test public void test03() {
		Tiles su = new Tiles(0,1,0,0,0);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(0,0), su);
		
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if tile that does not fit can be placed at north of another tile
	@Test public void test04() {
		Tiles su = new Tiles(1,1,1,1,1);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(0,1), su);
		//System.out.println(board.size());
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if tile that does not fit can be placed at south of another tile
	@Test public void test05() {
		Tiles su = new Tiles(1,1,1,1,1);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(0,-1), su);
		
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if tile that does not fit can be placed at east of another tile
	@Test public void test06() {
		Tiles su = new Tiles(1,1,1,1,1);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(1,0), su);
		
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if tile that does not fit can be placed at west of another tile
	@Test public void test07() {
		Tiles su = new Tiles(1,1,1,1,1);
		
		boolean expected = false;
		boolean actual = board.add(new Coordinates(-1,0), su);
		
		Assert.assertFalse("Expected: " + expected + " Actual: " + actual,
			actual);
	}
	
	//Test if tile that fits can be placed at north of another tile and expands board.
	@Test public void test08() {
		Tiles su = new Tiles(1,2,0,1,1);
		
		boolean added = board.add(new Coordinates(0,1), su);
		int expected = 8;
		int actual = board.size();
		
		Assert.assertTrue("Expected: " + expected + " Actual: " + actual + " Added: " + added,
				expected == actual && added);
	}
	
	//Test if tile that fits can be placed at south of another tile and expands board.
	@Test public void test09() {
		Tiles su = new Tiles(0,1,1,1,1);
		
		boolean added = board.add(new Coordinates(0,-1), su);
		int expected = 11;
		int actual = board.size();
		
		Assert.assertTrue("Expected: " + expected + " Actual: " + actual + " Added: " + added,
				expected == actual && added);
	}
	
	//Test if tile that fits can be placed at east of another tile and expands board.
	@Test public void test10() {
		Tiles su = new Tiles(1,1,1,0,1);
		
		boolean added = board.add(new Coordinates(1,0), su);
		int expected = 12;
		int actual = board.size();
		
		Assert.assertTrue("Expected: " + expected + " Actual: " + actual + " Added: " + added,
				expected == actual && added);
	}
	
	//Test if another tile can be placed at 0,1 and expands board.
	@Test public void test11() {
		
		Tiles su = new Tiles(0,0,0,0,0);
		
		boolean added = board.add(new Coordinates(0,1), su);
		int expected = 12;
		int actual = board.size();
		
		Assert.assertTrue("Expected: " + expected + " Actual: " + actual + " Added: " + added,
				expected == actual && !added);
	}
}
