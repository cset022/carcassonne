package test;
import code.TileBag;
import code.Tiles;
import org.junit.Test;
import org.junit.Assert;

public class createTileTest {

	TileBag tileBag = new TileBag();
	
	//test if tile will be created with correct values
	@Test
	public void test00()
	{
		
		Tiles tile = tileBag.getTile();
		
		boolean expected=true;
		boolean actual;
		
		if (checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.center()))
		{
			actual=true;
		}
		else
		{
			actual=false;
		}
		
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected==actual);
		
	}
	
	//test if tile will be created with correct values
	@Test
	public void test01()
	{
		
		Tiles tile = tileBag.getTile();
		
		boolean expected=true;
		boolean actual;
		
		if (checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.north()) && checkValue(tile.center()))
		{
			actual=true;
		}
		else
		{
			actual=false;
		}
		
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected==actual);
		
	}
	
	public static boolean checkValue(int v) {
		return v >= 0 && v <=4;
	}
	
	//test if tile instance will be created
	@Test
	public void test02()
	{
		
		Tiles tile = tileBag.getTile();
		
		boolean expected=true;
		boolean actual;
		
		if (tile == null)
		{
			actual=false;
		}
		else
		{
			actual=true;
		}
		
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected==actual);
		
	}

}
