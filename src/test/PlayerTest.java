package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Player;
import code.Player.NoMeeplesException;

public class PlayerTest {
	//name test
	@Test
	public void test00() {
		Player player = new Player("Paul");
		String expected = "Paul";
		assertTrue("Expected name: " + expected + " Actual name: " + player.name(),
				player.name().equals(expected));
	}
	//meeple test
	@Test
	public void test01() {
		Player player = new Player("Paul");
		int expected = 7;
		assertTrue("Expected meeples: " + expected + " Actual meeples: " + player.getMeeples(),
				player.getMeeples() == expected);
	}
	//addMeeple test
	@Test
	public void test02() {
		Player player = new Player("Paul");
		player.addMeeple();
		int expected = 8;
		assertTrue("Expected meeples: " + expected + " Actual meeples: " + player.getMeeples(),
				player.getMeeples() == expected);
	}
	//removeMeeple test
	@Test
	public void test03() {
		Player player = new Player("Paul");
		try {
			player.removeMeeple();
		} catch (NoMeeplesException e) {
			e.printStackTrace();
		}
		int expected = 6;
		assertTrue("Expected meeples: " + expected + " Actual meeples: " + player.getMeeples(),
				player.getMeeples() == expected);
	}
	//NoMeeplesException test
	@Test
	public void test04() {
		Player player = new Player("Paul");
		boolean actual = false;
		try {
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
			player.removeMeeple();
		} catch (NoMeeplesException e) {
			actual = true;
		}
		boolean expected = true;
		assertTrue("Expected meeples: " + expected + " Actual meeples: " + player.getMeeples(),
				actual == expected);
	}
	//add points test
	@Test
	public void test05() {
		Player player = new Player("Paul");
		player.addPoints(3);
		int expected = 3;
		assertTrue("Expected meeples: " + expected + " Actual meeples: " + player.getMeeples(),
				player.getPoints() == expected);
	}

}
