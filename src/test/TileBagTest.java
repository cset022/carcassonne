package test;
import code.TileBag;
import code.Tiles;
import org.junit.Test;
import org.junit.Assert;

public class TileBagTest {

	public static TileBag tileBag = new TileBag();
	
	//test TileBag size
	@Test
	public void test00() {
		int expected = 71;
		int actual = tileBag.size();
			
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected == actual);
	}
	
	//test hasTile
	@Test
	public void test01() {
		boolean expected = true;
		boolean actual = tileBag.hasTile();
			
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected == actual);
	}
	
	//test isEmpty
	@Test
	public void test02() {
		boolean expected = false;
		boolean actual = tileBag.isEmpty();
				
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected == actual);
	}
	
	//test getTile method
	@Test
	public void test03() {
		Tiles actual = tileBag.getTile();
		
		Assert.assertTrue("Actual: " + actual, actual != null);
		
	}
	
	//test if tile will be created with correct values
	@Test
	public void test04() {
		
		Tiles tile = tileBag.getTile();
		
		boolean expected = true;
		boolean actual = (checkValue(tile.north()) && checkValue(tile.east()) && checkValue(tile.south()) && checkValue(tile.west()) && checkValue(tile.center()));
		
		Assert.assertTrue("Expected: " + expected + "Actual: " + actual, expected==actual);
	}
	
	private static boolean checkValue(int v) {
		return v >= 0 && v <=5;
	}
	
	//test if TileBag will return null after all tiles are drawn
	@Test
	public void test05() {
		Tiles expected = null;
		Tiles actual = tileBag.getTile();
		int size = tileBag.size() + 1;
		
		while (size-- != 0) {
			actual = tileBag.getTile();
		}
		
		Assert.assertTrue("Expected: " + expected + "Actual: "
				+ actual + "isEmpty: " + tileBag.isEmpty(),
				expected == actual && tileBag.isEmpty());
	}

}
