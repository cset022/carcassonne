package test;

import static org.junit.Assert.*;

import org.junit.Test;

import code.Coordinates;
import code.Tiles;

public class CoordinatesTest {
	//constructor test
	@Test
	public void test00() {
		Coordinates c = new Coordinates(0,0);
		int expectedX = 0, expectedY = 0;
		assertTrue("Expected X: " + expectedX + "Expected Y: " + expectedY +
				" Actual X: " + c.x() + " Actual Y: " + c.y(),
				expectedX == c.x() && expectedY == c.y());
	}
	//constructor with tile test
	@Test
	public void test01() {
		Coordinates c = new Coordinates(-2,0, new Tiles(0,0,0,0,0));
		int expectedX = -2, expectedY = 0;
		assertTrue("Expected X: " + expectedX + "Expected Y: " + expectedY +
				" Actual X: " + c.x() + " Actual Y: " + c.y(),
				expectedX == c.x() && expectedY == c.y() && c.tile().equals(new Tiles(0,0,0,0,0)));
	}
	//add tile test
	@Test
	public void test02() {
		Coordinates c = new Coordinates(1,-1);
		int expectedX = 1, expectedY = -1;
		c.addTile(new Tiles(0,0,0,0,0));
		assertTrue("Expected X: " + expectedX + "Expected Y: " + expectedY +
				" Actual X: " + c.x() + " Actual Y: " + c.y(),
				expectedX == c.x() && expectedY == c.y() && c.tile().equals(new Tiles(0,0,0,0,0)));
	}
	//exception test
	@Test
	public void test03() {
		Coordinates c = new Coordinates(2,2);
		int expectedX = 2, expectedY = 2;
		boolean exc = false;
		c.addTile(new Tiles(0,0,0,0,0));
		try {
			c.addTile(new Tiles(0,0,0,0,0));
		} catch (IllegalArgumentException e) {
			exc = true;
		}
		assertTrue("Expected X: " + expectedX + "Expected Y: " + expectedY +
				" Actual X: " + c.x() + " Actual Y: " + c.y(),
				expectedX == c.x() && expectedY == c.y() 
				&& c.tile().equals(new Tiles(0,0,0,0,0)) && exc);
	}
	//toString test
	@Test
	public void test04() {
		Coordinates c = new Coordinates(-1,-1, new Tiles(0,0,0,0,0));
		int expectedX = -1, expectedY = -1;
		String expectedString = "-1,-1";
		assertTrue("Expected X: " + expectedX + "Expected Y: " + expectedY +
				" Actual X: " + c.x() + " Actual Y: " + c.y(),
				expectedX == c.x() && expectedY == c.y() && 
				c.tile().equals(new Tiles(0,0,0,0,0)) &&
				c.toString().equals(expectedString));
	}

}
