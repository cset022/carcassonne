package test;

import static org.junit.Assert.*;

import org.junit.Test;
import code.Tiles;

public class RotateTileTest {

	//test overridden equals method
	@Test
	public void test00() {
		Tiles actual = new Tiles(1,1,0,0,0);
		Tiles expected = new Tiles(1,1,0,0,0);
		
		assertTrue("North: " + actual.north() +
				"East: " + actual.east() +
				"South: " + actual.south() +
				"West: " + actual.west() +
				"Center: " + actual.center()
				, actual.equals(expected));
	}
	
	//test rotateClockwise
	@Test
	public void test01() {
		Tiles actual = new Tiles(1,1,0,0,0);
		Tiles expected = new Tiles(0,1,1,0,0);
		actual.rotateClockwise();
		
		assertTrue("North: " + actual.north() +
				"East: " + actual.east() +
				"South: " + actual.south() +
				"West: " + actual.west() +
				"Center: " + actual.center()
				, actual.equals(expected));
	}
	
	//test rotateCounterClockwise
	@Test
	public void test02() {
		Tiles actual = new Tiles(1,1,0,0,0);
		Tiles expected = new Tiles(1,0,0,1,0);
		actual.rotateCounterClockwise();
		
		assertTrue("North: " + actual.north() +
				"East: " + actual.east() +
				"South: " + actual.south() +
				"West: " + actual.west() +
				"Center: " + actual.center()
				, actual.equals(expected));
	}

}
